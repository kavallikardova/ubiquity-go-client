# SignedTx

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Tx** | **string** | The signed TX | 

## Methods

### NewSignedTx

`func NewSignedTx(tx string, ) *SignedTx`

NewSignedTx instantiates a new SignedTx object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSignedTxWithDefaults

`func NewSignedTxWithDefaults() *SignedTx`

NewSignedTxWithDefaults instantiates a new SignedTx object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTx

`func (o *SignedTx) GetTx() string`

GetTx returns the Tx field if non-nil, zero value otherwise.

### GetTxOk

`func (o *SignedTx) GetTxOk() (*string, bool)`

GetTxOk returns a tuple with the Tx field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTx

`func (o *SignedTx) SetTx(v string)`

SetTx sets Tx field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


