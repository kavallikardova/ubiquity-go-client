# \AccountsAPI

All URIs are relative to *https://ubiquity.api.blockdaemon.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetBalancesByAddress**](AccountsAPI.md#GetBalancesByAddress) | **Get** /{platform}/{network}/account/{address} | Balances Of Address
[**GetBalancesByAddresses**](AccountsAPI.md#GetBalancesByAddresses) | **Post** /{platform}/{network}/accounts | Balances Of Addresses
[**GetReportByAddress**](AccountsAPI.md#GetReportByAddress) | **Get** /{platform}/{network}/account/{address}/report | A financial report for an address between a time period. Default timescale is within the last 30 days
[**GetTxsByAddress**](AccountsAPI.md#GetTxsByAddress) | **Get** /{platform}/{network}/account/{address}/txs | Transactions Of Address



## GetBalancesByAddress

> map[string]map[string]interface{} GetBalancesByAddress(ctx, platform, network, address).Assets(assets).Execute()

Balances Of Address



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    address := "0x2E31B312290A01538514806Fbb857736ea4d5555" // string | Account address
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetBalancesByAddress(context.Background(), platform, network, address).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetBalancesByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBalancesByAddress`: map[string]map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetBalancesByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**address** | **string** | Account address | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBalancesByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

**map[string]map[string]interface{}**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBalancesByAddresses

> map[string]map[string]map[string]interface{} GetBalancesByAddresses(ctx, platform, network).AccountsObj(accountsObj).Assets(assets).Execute()

Balances Of Addresses



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    accountsObj := *openapiclient.NewAccountsObj() // AccountsObj | 
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetBalancesByAddresses(context.Background(), platform, network).AccountsObj(accountsObj).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetBalancesByAddresses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBalancesByAddresses`: map[string]map[string]map[string]interface{}
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetBalancesByAddresses`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBalancesByAddressesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accountsObj** | [**AccountsObj**](AccountsObj.md) |  | 
 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**map[string]map[string]map[string]interface{}**](map.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetReportByAddress

> Report GetReportByAddress(ctx, platform, network, address).From(from).To(to).Execute()

A financial report for an address between a time period. Default timescale is within the last 30 days



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    address := "0x2E31B312290A01538514806Fbb857736ea4d5555" // string | Account address
    from := int32(961846434) // int32 | Unix Timestamp from where to start (optional)
    to := int32(1119612834) // int32 | Unix Timestamp from where to end (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetReportByAddress(context.Background(), platform, network, address).From(from).To(to).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetReportByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetReportByAddress`: Report
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetReportByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**address** | **string** | Account address | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetReportByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **from** | **int32** | Unix Timestamp from where to start | 
 **to** | **int32** | Unix Timestamp from where to end | 

### Return type

[**Report**](Report.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTxsByAddress

> TxPage GetTxsByAddress(ctx, platform, network, address).Order(order).Continuation(continuation).Limit(limit).Assets(assets).Execute()

Transactions Of Address



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    address := "0x2E31B312290A01538514806Fbb857736ea4d5555" // string | Account address
    order := "order_example" // string | Pagination order (optional)
    continuation := "8185.123" // string | Continuation token from earlier response (optional)
    limit := int32(25) // int32 | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetTxsByAddress(context.Background(), platform, network, address).Order(order).Continuation(continuation).Limit(limit).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetTxsByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTxsByAddress`: TxPage
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetTxsByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**address** | **string** | Account address | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTxsByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **order** | **string** | Pagination order | 
 **continuation** | **string** | Continuation token from earlier response | 
 **limit** | **int32** | Max number of items to return in a response. Defaults to 25 and is capped at 100.  | 
 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**TxPage**](TxPage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

