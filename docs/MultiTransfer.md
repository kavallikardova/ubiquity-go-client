# MultiTransfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Inputs** | [**[]Utxo**](Utxo.md) |  | 
**Outputs** | [**[]Utxo**](Utxo.md) |  | 
**Currency** | [**Currency**](Currency.md) |  | 
**TotalIn** | **string** | Integer string in smallest unit (Satoshis) | 
**TotalOut** | **string** | Integer string in smallest unit (Satoshis) | 
**Unspent** | **string** | Integer string in smallest unit (Satoshis) | 

## Methods

### NewMultiTransfer

`func NewMultiTransfer(inputs []Utxo, outputs []Utxo, currency Currency, totalIn string, totalOut string, unspent string, ) *MultiTransfer`

NewMultiTransfer instantiates a new MultiTransfer object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMultiTransferWithDefaults

`func NewMultiTransferWithDefaults() *MultiTransfer`

NewMultiTransferWithDefaults instantiates a new MultiTransfer object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetInputs

`func (o *MultiTransfer) GetInputs() []Utxo`

GetInputs returns the Inputs field if non-nil, zero value otherwise.

### GetInputsOk

`func (o *MultiTransfer) GetInputsOk() (*[]Utxo, bool)`

GetInputsOk returns a tuple with the Inputs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInputs

`func (o *MultiTransfer) SetInputs(v []Utxo)`

SetInputs sets Inputs field to given value.


### GetOutputs

`func (o *MultiTransfer) GetOutputs() []Utxo`

GetOutputs returns the Outputs field if non-nil, zero value otherwise.

### GetOutputsOk

`func (o *MultiTransfer) GetOutputsOk() (*[]Utxo, bool)`

GetOutputsOk returns a tuple with the Outputs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOutputs

`func (o *MultiTransfer) SetOutputs(v []Utxo)`

SetOutputs sets Outputs field to given value.


### GetCurrency

`func (o *MultiTransfer) GetCurrency() Currency`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *MultiTransfer) GetCurrencyOk() (*Currency, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *MultiTransfer) SetCurrency(v Currency)`

SetCurrency sets Currency field to given value.


### GetTotalIn

`func (o *MultiTransfer) GetTotalIn() string`

GetTotalIn returns the TotalIn field if non-nil, zero value otherwise.

### GetTotalInOk

`func (o *MultiTransfer) GetTotalInOk() (*string, bool)`

GetTotalInOk returns a tuple with the TotalIn field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalIn

`func (o *MultiTransfer) SetTotalIn(v string)`

SetTotalIn sets TotalIn field to given value.


### GetTotalOut

`func (o *MultiTransfer) GetTotalOut() string`

GetTotalOut returns the TotalOut field if non-nil, zero value otherwise.

### GetTotalOutOk

`func (o *MultiTransfer) GetTotalOutOk() (*string, bool)`

GetTotalOutOk returns a tuple with the TotalOut field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalOut

`func (o *MultiTransfer) SetTotalOut(v string)`

SetTotalOut sets TotalOut field to given value.


### GetUnspent

`func (o *MultiTransfer) GetUnspent() string`

GetUnspent returns the Unspent field if non-nil, zero value otherwise.

### GetUnspentOk

`func (o *MultiTransfer) GetUnspentOk() (*string, bool)`

GetUnspentOk returns a tuple with the Unspent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnspent

`func (o *MultiTransfer) SetUnspent(v string)`

SetUnspent sets Unspent field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


