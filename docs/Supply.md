# Supply

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Maximum** | Pointer to **string** | Maximum supply | [optional] 
**Total** | Pointer to **string** | Total supply at block height, excluding burnt coins | [optional] 
**TotalCreated** | Pointer to **string** | Total coins created historically up until this block | [optional] 
**TotalBurnt** | Pointer to **string** | Total coins burnt historically up until this block | [optional] 
**Created** | Pointer to **string** | Coins created at this block | [optional] 

## Methods

### NewSupply

`func NewSupply() *Supply`

NewSupply instantiates a new Supply object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSupplyWithDefaults

`func NewSupplyWithDefaults() *Supply`

NewSupplyWithDefaults instantiates a new Supply object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMaximum

`func (o *Supply) GetMaximum() string`

GetMaximum returns the Maximum field if non-nil, zero value otherwise.

### GetMaximumOk

`func (o *Supply) GetMaximumOk() (*string, bool)`

GetMaximumOk returns a tuple with the Maximum field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaximum

`func (o *Supply) SetMaximum(v string)`

SetMaximum sets Maximum field to given value.

### HasMaximum

`func (o *Supply) HasMaximum() bool`

HasMaximum returns a boolean if a field has been set.

### GetTotal

`func (o *Supply) GetTotal() string`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *Supply) GetTotalOk() (*string, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *Supply) SetTotal(v string)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *Supply) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetTotalCreated

`func (o *Supply) GetTotalCreated() string`

GetTotalCreated returns the TotalCreated field if non-nil, zero value otherwise.

### GetTotalCreatedOk

`func (o *Supply) GetTotalCreatedOk() (*string, bool)`

GetTotalCreatedOk returns a tuple with the TotalCreated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalCreated

`func (o *Supply) SetTotalCreated(v string)`

SetTotalCreated sets TotalCreated field to given value.

### HasTotalCreated

`func (o *Supply) HasTotalCreated() bool`

HasTotalCreated returns a boolean if a field has been set.

### GetTotalBurnt

`func (o *Supply) GetTotalBurnt() string`

GetTotalBurnt returns the TotalBurnt field if non-nil, zero value otherwise.

### GetTotalBurntOk

`func (o *Supply) GetTotalBurntOk() (*string, bool)`

GetTotalBurntOk returns a tuple with the TotalBurnt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotalBurnt

`func (o *Supply) SetTotalBurnt(v string)`

SetTotalBurnt sets TotalBurnt field to given value.

### HasTotalBurnt

`func (o *Supply) HasTotalBurnt() bool`

HasTotalBurnt returns a boolean if a field has been set.

### GetCreated

`func (o *Supply) GetCreated() string`

GetCreated returns the Created field if non-nil, zero value otherwise.

### GetCreatedOk

`func (o *Supply) GetCreatedOk() (*string, bool)`

GetCreatedOk returns a tuple with the Created field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreated

`func (o *Supply) SetCreated(v string)`

SetCreated sets Created field to given value.

### HasCreated

`func (o *Supply) HasCreated() bool`

HasCreated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


