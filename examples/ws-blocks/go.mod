module gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples/ws-blocks

go 1.13

require gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0

replace gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0 => ../../
