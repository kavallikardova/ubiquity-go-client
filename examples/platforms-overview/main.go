package main

import (
	"context"
	"fmt"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/client"
	"os"
)

/**
Fetching the full list of supported platforms.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and platform
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	platformsOverview, _, err := apiClient.PlatformsAPI.GetPlatforms(ctx).Execute()
	if err != nil {
		panic(fmt.Errorf("failed to get platforms overview: %v", err))
	}
	fmt.Println("Supported platforms:")
	for _, p := range platformsOverview.GetPlatforms() {
		fmt.Printf(" - %s\n   %s\n", p.GetHandle(), p.GetNetwork())
	}
}
