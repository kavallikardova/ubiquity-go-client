package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/client"
	ubiquitytx "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/tx"
	ubiquityws "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/ws"
	"math/big"
	"os"
	"time"
)

/**
Submitting a transaction and waiting for its confirmation.
*Important* for BTC, any amount left after sending to destination and change addresses is automatically paid as fee.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
	3) UBI_PLATFORM - optional, platform e.g. ethereum (bitcoin by default)

Flags for Bitcoin:
  -changeAddr string
        Change address for the amount left after transferring transaction value and leaving a fee
  -changeAmount int
        Change amount, sat.
  -destinationAddr string
        Destination address
  -destinationAmount int
        Destination amount, sat.
  -inputIndex uint
        Input index
  -inputTx string
        Input transaction
  -network string
        Network
  -privateKey string
        Private key

Flags for Ethereum:
  -destinationAddr string
        Destination address
  -destinationAmount int
        Destination amount
  -network string
        Network
  -nonce uint
        Nonce (current count of transactions)
  -privateKey string
        Private key
*/

const (
	txWaitDuration = 15 * time.Minute
)

func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and platform
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	// Use ubiquity.PlatformsAPI.GetPlatforms(ctx _context.Context) to fetch all supported platforms
	// See examples/platforms-overview
	var pl string
	if pl = os.Getenv("UBI_PLATFORM"); pl == "" {
		pl = "bitcoin"
	}

	txService := ubiquitytx.NewService(apiClient.TransactionsAPI)
	var network, address, txID string
	switch pl {
	case "bitcoin":
		network, address, txID = sendBTC(ctx, txService)
	case "ethereum":
		network, address, txID = sendETH(ctx, txService)
	default:
		panic(fmt.Errorf("protocoal '%s' is not supported yet", pl))
	}

	wsClient, err := ubiquityws.NewClient(&ubiquityws.Config{
		Platform: pl,
		Network:  network,
		APIKey:   accessToken,
	})
	if err != nil {
		panic(fmt.Errorf("failed to create a WS client: %v", err))
	}
	defer wsClient.Close()

	waitForTxConfirmation(wsClient, address, txID)
}

func sendBTC(ctx context.Context, txService *ubiquitytx.UbiquityTransactionService) (string, string, string) {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	inputTx := fs.String("inputTx", "", "Input transaction")
	inputIndex := fs.Uint("inputIndex", 0, "Input index")
	destinationAddr := fs.String("destinationAddr", "", "Destination address")
	destinationAmount := fs.Int64("destinationAmount", 0, "Destination amount, sat.")
	changeAddr := fs.String("changeAddr", "", "Change address")
	changeAmount := fs.Int64("changeAmount", 0, "Change amount, sat.")
	privKey := fs.String("privateKey", "", "Private key")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse BTC transaction flags: %v", err))
	}
	fs.VisitAll(createRequiredFlagsChecker(fs))

	result, err := txService.SendBTC(ctx, &ubiquitytx.BitcoinTransaction{
		Network: *network,
		From:    []ubiquitytx.TxInput{{Source: *inputTx, Index: uint32(*inputIndex)}},
		To: []ubiquitytx.TxOutput{
			{Destination: *destinationAddr, Amount: *destinationAmount},
			{Destination: *changeAddr, Amount: *changeAmount},
		},
		PrivateKey: *privKey,
	})
	if err != nil {
		panic(fmt.Errorf("failed to send ETH transaction: %v", err))
	}

	fmt.Printf("Transaction Hash = %s and ID = %s\n", result.TxHash, result.TxID)

	return *network, *destinationAddr, result.TxID
}

func sendETH(ctx context.Context, txService *ubiquitytx.UbiquityTransactionService) (string, string, string) {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	nonce := fs.Uint64("nonce", 0, "Nonce (current transactions count)")
	destinationAddr := fs.String("destinationAddr", "", "Destination address")
	destinationAmount := fs.Int64("destinationAmount", 0, "Destination amount")
	privKey := fs.String("privateKey", "", "Private key")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse BTC transaction flags: %v", err))
	}
	fs.VisitAll(createRequiredFlagsChecker(fs))

	result, err := txService.SendETH(ctx, &ubiquitytx.EthereumTransaction{
		Network:    *network,
		PrivateKey: *privKey,
		To:         *destinationAddr,
		Amount:     big.NewInt(*destinationAmount),
		Nonce:      *nonce,
	})
	if err != nil {
		panic(fmt.Errorf("failed to send BTC transaction: %v", err))
	}
	fmt.Printf("Transaction Hash = %s and ID = %s\n", result.TxHash, result.TxID)

	return *network, *destinationAddr, result.TxID
}

func createRequiredFlagsChecker(fs *flag.FlagSet) func(f *flag.Flag) {
	return func(f *flag.Flag) {
		if f.Value.String() == "" {
			fs.SetOutput(os.Stderr)
			_, _ = fmt.Fprintln(os.Stderr, "All of the flags are required:")
			fs.PrintDefaults()
			os.Exit(1)
		}
	}
}

func waitForTxConfirmation(wsClient *ubiquityws.Client, address, txID string) {
	subID, txs, err := wsClient.SubscribeTxs(&ubiquityws.TxsFilter{Addresses: []string{address}})
	if err != nil {
		panic(fmt.Errorf("failed to subscribe to transactions: %v", err))
	}
	fmt.Println("Subscribed for transactions of address", address)

	startedAt := time.Now()
	fmt.Println("Waiting for transaction confirmation...")
	newBlocks := make(map[string]bool) // This is just for fancy output
	for {
		select {
		case tx, ok := <-txs:
			if !ok {
				// The subscription would close if we close a client or get disconnected
				panic(errors.New("the subscription was closed"))
			}

			blockId := tx.GetBlockId()
			if !newBlocks[blockId] {
				newBlocks[blockId] = true
				fmt.Printf("Got new block #%s, checking...\n", blockId)
			}

			if tx.GetId() == txID {
				fmt.Printf("Transaction was confirmed under block #%s!\n", blockId)
				// Can be omitted if you close a client
				if err := wsClient.UnsubscribeTxs(subID); err != nil {
					panic(err)
				}
				return
			}
		case <-time.After(startedAt.Add(txWaitDuration).Sub(time.Now())):
			panic(fmt.Errorf("timed out while waiting for transaction confirmation"))
		}
	}
}
