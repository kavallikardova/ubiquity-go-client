module gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples/tx-send

go 1.13

require gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0

replace gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0 => ../../
