package ubiquityws

import (
	"encoding/json"
	"errors"
	"fmt"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/client"
)

type wsRequest struct {
	ID     int64                  `json:"id"`
	Method string                 `json:"method"`
	Params map[string]interface{} `json:"params"`
}

func newSubscribeRequest(reqID int64, channel string, details map[string]interface{}) *wsRequest {
	return &wsRequest{
		ID:     reqID,
		Method: "ubiquity.subscribe",
		Params: map[string]interface{}{
			"channel": channel,
			"detail":  details,
		},
	}
}

func newUnsubscribeRequest(reqID int64, channel string, subID int64) *wsRequest {
	return &wsRequest{
		ID:     reqID,
		Method: "ubiquity.unsubscribe",
		Params: map[string]interface{}{
			"channel": channel,
			"subID":   subID,
		},
	}
}

type wsResponse struct {
	ID     int64                  `json:"id"`
	Result map[string]interface{} `json:"result"`
	Error  map[string]interface{} `json:"error"`
}

func wsResponseFromRaw(msg rawJSONValues) (*wsResponse, error) {
	reqID, err := msg.getInt64("id")
	if err != nil {
		return nil, fmt.Errorf("wsResponse unmarshalling failure: %v", err)
	}
	resp := &wsResponse{ID: reqID}
	if msg.hasKey("result") {
		resp.Result = make(map[string]interface{})
		if err := msg.getForRef("result", &resp.Result); err != nil {
			return nil, fmt.Errorf("wsResponse unmarshalling failure: %v", err)
		}
	} else if msg.hasKey("error") {
		resp.Error = make(map[string]interface{})
		if err := msg.getForRef("error", &resp.Error); err != nil {
			return nil, fmt.Errorf("wsResponse unmarshalling failure: %v", err)
		}
	}
	return resp, nil
}

type wsNotificationParams struct {
	Items []*wsNotificationParamsItem `json:"items"`
}

type wsNotificationParamsItem struct {
	SubID   int64       `json:"subID"`
	Channel string      `json:"channel"`
	Revert  bool        `json:"revert"`
	Content interface{} `json:"content"`
}

func (w *wsNotificationParams) UnmarshalJSON(bytes []byte) error {
	msg := make(rawJSONValues)
	if err := json.Unmarshal(bytes, &msg); err != nil {
		return err
	}

	if msg.hasKey("items") {
		rawItems := make([]rawJSONValues, 0)
		if err := msg.getForRef("items", &rawItems); err != nil {
			return err
		}

		for _, ri := range rawItems {
			item := new(wsNotificationParamsItem)
			var err error

			item.SubID, err = ri.getInt64("subID")
			if err != nil {
				return err
			}
			item.Channel, err = ri.getString("channel")
			if err != nil {
				return err
			}
			item.Revert, err = ri.getBool("revert")
			if err != nil {
				return err
			}

			switch item.Channel {
			case blockIDChannel:
				item.Content = new(ubiquity.BlockIdentifier)
			case blockChannel:
				item.Content = new(ubiquity.Block)
			case txChannel:
				item.Content = new(ubiquity.Tx)
			default:
				return fmt.Errorf("unsupported channel type: %s", item.Channel)
			}
			if err := ri.getForRef("content", item.Content); err != nil {
				return err
			}

			w.Items = append(w.Items, item)
		}
	} else {
		return errors.New("missing required field 'items'")
	}

	return nil
}
