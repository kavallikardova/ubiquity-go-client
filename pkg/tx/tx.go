package ubiquitytx

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/client"
	"math/big"
	"strconv"
	"strings"
)

const (
	bitcoinMainnet = "mainnet"
	bitcoinTestnet = "testnet"

	ethereumMainnet = "mainnet"
	ethereumRopsten = "ropsten"

	ethereumTxGasLimit = 21000 // Default for simple transfer transaction
)

var (
	ethereumChains = map[string]*big.Int{
		ethereumMainnet: big.NewInt(1),
		ethereumRopsten: big.NewInt(3),
	}
)

// UbiquityTransactionService provides API to send various protocol transactions.
// So far it supports BTC and ETH.
type UbiquityTransactionService struct {
	txAPI ubiquity.TransactionsAPI
}

func NewService(txAPI ubiquity.TransactionsAPI) *UbiquityTransactionService {
	return &UbiquityTransactionService{
		txAPI: txAPI,
	}
}

type TxInput struct {
	Source string // UTXO, input transaction id
	Index  uint32 // UTXO, input index
}

type TxOutput struct {
	Destination string // Destination address
	Amount      int64  // In sat.
}

// BitcoinTransaction *Important* make sure to properly calculate the difference between INs and OUTs
// since it will be automatically payed as a Fee.
// In the future we may include UTXO and Fee calculation into the service.
type BitcoinTransaction struct {
	Network string
	From    []TxInput
	To      []TxOutput
	// Fee        int64
	// ChangeAddr string
	PrivateKey string
}

// Struct validation wasn't used in order to eliminate extra dependencies for SDK users.
func (t BitcoinTransaction) validate() error {
	if strings.TrimSpace(t.Network) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if len(t.From) == 0 {
		return fmt.Errorf("field 'From' must not be empty")
	}
	for _, in := range t.From {
		if in.Source == "" {
			return fmt.Errorf("input fields 'Source' and 'Index' must be set")
		}
	}
	if len(t.To) == 0 {
		return fmt.Errorf("field 'To' must not be empty")
	}
	for _, out := range t.To {
		if out.Destination == "" {
			return fmt.Errorf("output fields 'Destination' and 'Amount' must be set")
		}
	}
	if strings.TrimSpace(t.PrivateKey) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	return nil
}

type SendBTCResult struct {
	TxHash string
	TxID   string
}

// SendBTC creates, signs and sends BTC transaction. Under the hood it uses Ubiquity TxSend API.
func (s UbiquityTransactionService) SendBTC(ctx context.Context, tx *BitcoinTransaction) (*SendBTCResult, error) {
	if err := tx.validate(); err != nil {
		return nil, fmt.Errorf("Bitcoin Transaction validation failure: %v", err)
	}

	msgTx := wire.NewMsgTx(wire.TxVersion)

	for _, in := range tx.From {
		txHash, err := chainhash.NewHashFromStr(in.Source)
		if err != nil {
			return nil, fmt.Errorf("failed to get hash from source string: %v", err)
		}
		msgTx.AddTxIn(wire.NewTxIn(wire.NewOutPoint(txHash, in.Index), nil, nil))
	}

	netParams := &chaincfg.MainNetParams
	if tx.Network == bitcoinTestnet {
		netParams = &chaincfg.TestNet3Params
	}
	for _, out := range tx.To {
		destAddr, err := btcutil.DecodeAddress(out.Destination, netParams)
		if err != nil {
			return nil, fmt.Errorf("failed to decode destination address: %v", err)
		}
		pkScript, err := txscript.PayToAddrScript(destAddr)
		if err != nil {
			return nil, fmt.Errorf("failed to build P2PKH script: %v", err)
		}

		msgTx.AddTxOut(wire.NewTxOut(out.Amount, pkScript))
	}

	signedTx, err := signBitcoinTx(tx.PrivateKey, msgTx, netParams)
	if err != nil {
		return nil, fmt.Errorf("failed to sign Bitcoin Transaction: %v", err)
	}

	txReceipt, resp, err := s.txAPI.TxSend(ctx, "bitcoin", tx.Network).
		SignedTx(ubiquity.SignedTx{Tx: signedTx}).Execute()
	if err != nil {
		return nil, fmt.Errorf("Ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	return &SendBTCResult{
		TxHash: msgTx.TxHash().String(),
		TxID:   txReceipt.GetId(),
	}, nil
}

func signBitcoinTx(privKey string, msgTx *wire.MsgTx, netParams *chaincfg.Params) (string, error) {
	wif, err := btcutil.DecodeWIF(privKey)
	if err != nil {
		return "", fmt.Errorf("failed to decode WIF: %v", err)
	}
	pubKey, err := btcutil.NewAddressPubKey(wif.PrivKey.PubKey().SerializeUncompressed(), netParams)
	if err != nil {
		return "", fmt.Errorf("failed to build pubKey: %v", err)
	}
	pubKeyAddress, err := btcutil.DecodeAddress(pubKey.EncodeAddress(), netParams)
	if err != nil {
		return "", fmt.Errorf("failed to decode address: %v", err)
	}
	inputPKScript, err := txscript.PayToAddrScript(pubKeyAddress)
	if err != nil {
		return "", fmt.Errorf("failed to build P2PKH script: %v", err)
	}

	for i, in := range msgTx.TxIn {
		sigScript, err := txscript.SignatureScript(msgTx, i, inputPKScript, txscript.SigHashAll, wif.PrivKey, false)
		if err != nil {
			return "", fmt.Errorf("failed to sign: %v", err)
		}
		in.SignatureScript = sigScript
	}

	var signedTx bytes.Buffer
	_ = msgTx.Serialize(&signedTx)
	encodedSignedTx := hex.EncodeToString(signedTx.Bytes())

	return encodedSignedTx, nil
}

type EthereumTransaction struct {
	Network    string
	PrivateKey string
	To         string
	Amount     *big.Int // In wei
	GasLimit   uint64   // Optional, 21000 by default
	GasPrice   *big.Int // Optional, in wei, estimated using "eth_gasPrice" if not set
	Nonce      uint64   // Current transaction count
}

func (t EthereumTransaction) validate() error {
	if strings.TrimSpace(t.Network) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if strings.TrimSpace(t.PrivateKey) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	if strings.TrimSpace(t.To) == "" {
		return fmt.Errorf("field 'To' is required")
	}
	if t.Amount == nil {
		return fmt.Errorf("field 'Amount' is required")
	}
	return nil
}

type SendETHResult struct {
	TxHash string
	TxID   string
}

// SendETH creates, signs and sends ETH transaction. Under the hood it uses Ubiquity TxSend API.
func (s UbiquityTransactionService) SendETH(ctx context.Context, tx *EthereumTransaction) (*SendETHResult, error) {
	if err := tx.validate(); err != nil {
		return nil, fmt.Errorf("Ethereum Transaction validation failure: %v", err)
	}

	privKey, err := crypto.HexToECDSA(tx.PrivateKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key: %v", err)
	}

	pubKey, ok := privKey.Public().(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("unexpected public key type: %T", pubKey)
	}

	toAddr := common.HexToAddress(tx.To)
	var gasLimit uint64 = ethereumTxGasLimit
	if tx.GasLimit > 0 {
		gasLimit = tx.GasLimit
	}

	gasPrice := tx.GasPrice
	if gasPrice == nil {
		gasPrice, err = s.estimateGasPrice(ctx, tx.Network)
		if err != nil {
			return nil, err
		}
	}

	legTx := types.NewTx(&types.LegacyTx{
		Nonce:    tx.Nonce,
		GasPrice: gasPrice,
		Gas:      gasLimit,
		To:       &toAddr,
		Value:    tx.Amount,
	})

	chainID, ok := ethereumChains[tx.Network]
	if !ok {
		return nil, fmt.Errorf("network %s is not supported", tx.Network)
	}

	signedTx, err := types.SignTx(legTx, types.NewEIP155Signer(chainID), privKey)
	if err != nil {
		return nil, fmt.Errorf("failed to sign: %v", err)
	}

	signedTxData, err := signedTx.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("failed to marshal: %v", err)
	}

	txReceipt, resp, err := s.txAPI.TxSend(ctx, "ethereum", tx.Network).
		SignedTx(ubiquity.SignedTx{Tx: hex.EncodeToString(signedTxData)}).Execute()
	if err != nil {
		return nil, fmt.Errorf("Ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	return &SendETHResult{
		TxHash: signedTx.Hash().String(),
		TxID:   txReceipt.GetId(),
	}, nil
}

func (s UbiquityTransactionService) estimateGasPrice(ctx context.Context, network string) (*big.Int, error) {
	feeStr, resp, err := s.txAPI.EstimateFee(ctx, "ethereum", network).Execute()
	if err != nil {
		return nil, fmt.Errorf("Ubiquity EstimateFee failure: resp. status = '%v' and err = '%v'", resp.Status,
			err)
	}
	feeStr = strings.Trim(feeStr, "\"") // TODO: remove after we fix this
	fee, err := strconv.ParseInt(feeStr, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("failed to parse fee: %v", err)
	}
	return big.NewInt(fee), nil
}
